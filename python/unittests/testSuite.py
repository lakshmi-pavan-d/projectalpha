'''
Created on May 22, 2015
Unit tests
@author: pdaggubati
'''
import unittest
from datetime import *
from TDAPI import *
from StockData import *
from Utils import *
from stockAnalytics import *

class ProjectAlphaTests(unittest.TestCase):
    
    def test_StockData1(self):
        s = StockData('GOOG', reportDt = datetime(2015,5,1))
        s.data.pop('events'); s.data.pop('openGF'); s.data.pop('lastGF')
        s.data.pop('name'); s.data.pop('high52wk'); s.data.pop('low52wk')        
        self.assertEqual(s.data,{'prior_year_close': 526.39999999999998, 'prior_week_close': 565.05999999999995, 
                                 'prev_close': 537.34000000000003, 'high': 539.53999999999996, 
                                 'close': 537.89999999999998, 'open': 538.42999999999995, 'low': 532.10000000000002})
    def test_StockData2(self):
        s = StockData('AAPL')
        data = s.data
        print data
        self.assertFalse(np.isnan(data['openGF']) | np.isnan(data['lastGF']) 
                        | np.isnan(data['low52wk']) | np.isnan(data['high52wk'])
                        )
    

        
if __name__ == '__main__':
    unittest.main()
