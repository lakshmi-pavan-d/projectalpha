import urllib, datetime
import numpy as np
from datetime import *
import lxml.html as html
from TDAPI import TDAPI

# Generic class to pull data from Google finance
class StockData:
    def __init__(self, symbol, tdAPI=None, reportDt = datetime.today().strftime('%Y%m%d'), histDates = [], histData = {}):
        # if histDates = [], only prior day OHLC is retrieved, else for all dates
        self.base_url = 'https://www.google.com/finance?q='
        self.symbol = symbol
        self.date   = reportDt
        self.gfData =  {}
        self.histData = {}
        self.parsed_html = ""
        self.url = self.base_url + symbol
        
        if tdAPI == None:
            self.tdAPI = TDAPI()
            self.tdAPI.LogIn()
            
        self.parseHtml()
        self.getName()
        self.getEvents()
        self.getPrices() # Google API 52 week high/low
        self.getHistPrices(histDates, histData) #TD API prices
        
    def parseHtml(self):
        sock = urllib.urlopen(self.url)
        doc = sock.read()
        self.parsed_html = html.fromstring(doc)

    def getName(self):
        # name comes along with other string
        # e.g. Xilinx, Inc.: NASDAQ:XLNX quotes & news - Google Finance
        name = self.parsed_html.find(".//title").text
        idx  = name.find(':')-1
        self.gfData['name'] = name[:idx];
    
    def getEvents(self):
        events = self.parsed_html.findall(".//div[@class='event']")
        self.gfData['events']={}    
        for e in events:
            dt = (e[0].text)
            desc = (e[1].text)
            self.gfData['events'][dt] = desc
                    
    def getPrices(self):
        
        # Prices from parsing the website
        t = self.parsed_html.find(".//table[@class='snap-data']")   
        t1 = self.parsed_html.xpath('//*[@id="price-panel"]/div[1]/span')
        
        try:
            range52Wk = t.getchildren()[1].getchildren()[1].text.split(' - ')
            self.gfData['high52wk'] = float(range52Wk[1].strip().replace(',',''))
            self.gfData['low52wk'] = float(range52Wk[0].strip().replace('',''))
            self.gfData['openGF'] = float(t.getchildren()[2].getchildren()[1].text.strip().replace(',',''))
            self.gfData['lastGF'] = float(t1[0].getchildren()[0].text.strip().replace(',',''))
        except:
            print "GF ERROR: unable to retrieve prices for stock %s"%self.symbol
            self.gfData['high52wk'] = np.NaN
            self.gfData['low52wk'] = np.NaN
            self.gfData['openGF'] = np.NaN
            self.gfData['lastGF'] = np.NaN       

    def getPrices2(self):
        
        # Prices from Google Finance API(real-time)
        pass
      


    def getHistPrices(self, histDate, histData):
        # Historical prices from TD API
        if histDate ==[]:
            return
            #histDate.append(self.date)
            
        for dt in histDate:
            symbol = self.symbol
            self.histData[dt]= {} 
            
            # if data is already passed, no need for web call
            if not histData.has_key(dt):
                # TD API price history call for OHLC, prev_close, prior_year_close
                tdAPI = self.tdAPI
                symbol = self.symbol
                status = tdAPI.PriceHistory([self.symbol], 'DAILY', 1, startdate = dt, enddate = dt)
                histData[dt] = tdAPI.data
            
            if histData[dt][symbol]['status'] == 'SUCCESS':
                data = histData[dt][symbol]['data']
                new_data = {}
                for key in data.keys():
                    new_data[key.strftime('%Y%m%d')] = data[key]
                
                if new_data.has_key(dt):
                    self.histData[dt]['open']           = np.around(new_data[dt]['open'],2)
                    self.histData[dt]['close']          = np.around(new_data[dt]['close'],2)
                    self.histData[dt]['high']           = np.around(new_data[dt]['high'],2)   
                    self.histData[dt]['low']            = np.around(new_data[dt]['low'],2)
                else:
                    print "TD ISSUE: Unable to retrieve OHLC for stock %s; date %s"%(symbol, dt)
                    self.histData[dt]['open']           = np.around(self.gfData['openGF'],2)
                    self.histData[dt]['close']          = np.around(self.gfData['lastGF'],2)
                                        
            else:
                print "TD ERROR: unable to retrieve price as of %s for %s"%(dt,symbol)
                self.histData[dt] = {'open': np.NaN, 'close': np.NaN, 'high': np.NaN, 'low':np.NaN, 'prev_close':np.NaN}
            
if __name__ == "__main__":
    symbol = 'GPRO'
    s = StockData(symbol,histDates=['20150702','20150701'])
    print s.histData
    print s.gfData
