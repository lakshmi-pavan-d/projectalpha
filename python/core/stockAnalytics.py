import sys
sys.path.insert(0,'..')
import pandas as pd
import StockData as SD
from utils.Utils import *
from datetime import datetime
from datetime import timedelta
from TDAPI import TDAPI

threshold = 0.10
pd.set_option('display.max_colwidth', 150)


class StockAnalytics():
    
    def __init__(self):
        self.tdAPI = TDAPI()
        self.tdAPI.LogIn()
    
    def __del__(self):
        self.tdAPI.LogOut()

    def getStockData(self,symbols, reportDt, histDates=[]):        
        rDt         = reportDt
        today       = rDt.strftime('%Y%m%d')
        priorday    = priorBDay(dt=rDt,nPrior=-1).strftime('%Y%m%d')
        priorweek   = priorBDay(dt=rDt,nPrior=-5).strftime('%Y%m%d')
        priormonth  = priorBDay(dt=rDt,nPrior=-22).strftime('%Y%m%d')
        priorquarter= priorBDay(dt=rDt,nPrior=-64).strftime('%Y%m%d')
        prioryear   = priorBDay(dt=rDt,nPrior=-252).strftime('%Y%m%d')
        prior2year  = priorBDay(dt=rDt,nPrior=-504).strftime('%Y%m%d')
        prior5year  = priorBDay(dt=rDt,nPrior=-1260).strftime('%Y%m%d')
    
        if histDates == []:        
            histDates   = [today, priorday, priorweek, priormonth, priorquarter, prioryear, prior2year, prior5year]
    
        # Call TD API for all stocks at once. Making too many calls is not stable
        dataOHLC={}
        for dt in histDates:
            status = self.tdAPI.PriceHistory(symbols, 'DAILY', 1, startdate = dt, enddate = dt)
            dataOHLC[dt] = self.tdAPI.data
        
        stockData = []
        for sy in symbols:
            print "Gathering data for stock %s" %sy
            s = SD.StockData(sy, self.tdAPI, reportDt=reportDt, histDates = histDates, histData = dataOHLC)
            stockData.append(s)
        return stockData
    
    def getCorpEvents(self, stockData, startDate="", endDate = "",pFlag = False):
        # Filter events for the given date range        
        
        print 'Generating corporate events report'
        # Defaults today and 14 days ahead
        if startDate == "":
            startDate = datetime.today();
        else:
            startDate = datetime.strptime(startDate,'%Y%m%d')
        
        if endDate == "":
            endDate = startDate + timedelta(days=14)
        else:
            endDate = datetime.strptime(endDate,'%Y%m%d')
        
        print '(startDate, endDate)',(startDate, endDate)
        data_dict = {}
        for s in stockData:
            sy = s.symbol
            if pFlag: print s.gfData['events']        
            for dt in s.gfData['events'].keys():
                # if the event date falls within the range and has Events keyword, include it
                if datetime.strptime(dt, '%b %d, %Y') > startDate and datetime.strptime(dt, '%b %d, %Y') <= endDate:
                    if (s.gfData['events'][dt].lower()).find('earnings') != -1:
                        data_dict[sy] = {}
                        data_dict[sy]['name']       = s.gfData['name']
                        data_dict[sy]['date']       = datetime.strptime(dt, '%b %d, %Y')
                        data_dict[sy]['event']      = s.gfData['events'][dt].strip(' \n-')
                        data_dict[sy]['today_close'] = s.gfData['lastGF']
                        data_dict[sy]['52wk_high']  = s.gfData['high52wk']
                        data_dict[sy]['52wk_low']   = s.gfData['low52wk']
                        print "Earnings event found for stock " + sy
                        pass
    
        if data_dict != {}:
            dataF = pd.DataFrame.from_dict(data_dict,orient='index').sort(columns = ['date'], axis = 0) 
            return dataF.reindex_axis(['name','date','event','today_close','52wk_low','52wk_high'],axis=1)
        else:
            return pd.DataFrame.from_dict(data_dict,orient='index')
    
    def get52WkHighLowStocks(self, stockData,th = threshold):
        # stocks near 52wk high or low        
        print 'Generating 52 week high low report'
        data_dict = {}
        for s in stockData:
            sy = s.symbol
            data_dict[sy] = [s.gfData['name'],s.gfData['lastGF'], s.gfData['high52wk'], s.gfData['low52wk'], 
                             (s.gfData['high52wk'] - s.gfData['lastGF'])/s.gfData['lastGF'],  
                             (s.gfData['lastGF'] - s.gfData['low52wk'])/s.gfData['lastGF']
                             ]
    
        df = pd.DataFrame.from_dict(data_dict, 'index')
        df.dropna(inplace=True)
        df.columns = ['name','today_close','high_52wk','low_52wk', 'flag_high', 'flag_low']
        df_high = df[df.flag_high < threshold].drop(['flag_high','flag_low'],axis=1)
        df_low = df[df.flag_low < threshold].drop(['flag_high','flag_low'],axis=1)
    
        return [df_high, df_low]
    
    def getMovers(self, stockData,sd, ed, nThres):
        print "Generating top movers in a given a time period"
        data_dict = {}
            
        for s in stockData:
            sy = s.symbol
            prior_close = s.histData[ed]['close']
            today_close = s.histData[sd]['close']
            data_dict[sy] = [s.gfData['name'], today_close, prior_close,s.gfData['high52wk'], s.gfData['low52wk'], 
                             (today_close - prior_close)/prior_close]
            
        df = pd.DataFrame.from_dict(data_dict, 'index')
        df.dropna(inplace=True)
        df.columns  = ['name','today_close', 'prior_close', 'high_52wk','low_52wk', 'period_return']
        df_filter = df[abs(df.period_return) > nThres]
        df_filter.sort('period_return',axis=0,inplace=True, ascending=False)
        return df_filter
    
    def getCurrentPositions(self):
        api = self.tdAPI
        status = api.BalancesAndPositions(type='p')
        
        if api.status!='FAIL':
            df = pd.DataFrame.from_dict(api.positions,orient='index')  
            df= df[['name','position_type','quantity','avg_buy_price','last_price','day_return','total_return']]
            df.sort('total_return', axis=0, inplace=True, ascending=False)
            return df
        else:
            return None
    
    def getLastTrades(self,startDate, endDate):
        status = self.tdAPI.TradesHistory(type=1,startdate=startDate, enddate = endDate)
        self.tradeHistory = self.tdAPI.tradeHistory
        
        if self.tdAPI.status != 'FAIL':
            df = self.tdAPI.tradeHistory
            df = df[['symbol','executedDate','buySellCode','price']]
            df.columns = ['symbol','priorTradeDate','buySell','priorTradePrice']
            
            # get most recent trade for each stock
            df1 = df[['symbol','priorTradeDate']].groupby('symbol').max().reset_index()
            final = pd.merge(df1,df,on=['symbol','priorTradeDate'],how='left')
            final['priorTradeDate'] = final['priorTradeDate'].map(lambda x: x.strftime('%Y-%m-%d'))
            final = final.groupby(['symbol','priorTradeDate','buySell']).mean().reset_index()
            final.set_index(['symbol'],inplace=True)
            return final
        else:
            return None

    def findInvalid(self,hist,symb):
        shares = 0.0
        for i in range(len(hist["symbol"])):
            if hist["symbol"][i] == symb:
                if hist["buySellCode"][i] == 'B':
                    shares += self.tradeHistory["quantity"][i]
                else:
                   shares -= self.tradeHistory["quantity"][i]
        return shares == 0

    def getProfitLoss(self, start_date, end_date):
        count = 0.0
        status = self.tdAPI.TradesHistory(type=1,startdate=start_date, enddate = end_date)
        self.tradeHistory = self.tdAPI.tradeHistory
        self.currPositions = self.getCurrentPositions().reset_index()
        for i in range(len(self.tradeHistory["symbol"])):
            if self.findInvalid(self.tradeHistory,self.tradeHistory["symbol"][i]):
                count += self.tradeHistory["value"][i]
        return count


    
if __name__ == "__main__":    
        
    """
    # Read input list
    f = open(csv_f,'r')
    tickers = f.read().split('\n')
    f.close()
    dataF = getStockData(tickers)
   
    msg = '<b>Stocks near 52 Week Low</b>'
    msg += dataF[1].to_html(justify='left') + "<br />"
    msg += '<b>Stocks near 52 Week High</b>'
    msg += dataF[0].to_html(justify='left') + "<br />"

    subject = "Stocks trading near 52 week low/high - " + datetime.today().strftime('%b %d, %Y')   
    
    Utils.sendHTMLEmail(from_addr= 'pavan_daggubati@yahoo.co.in', 
          to_addr_list = ['alpha_investing@googlegroups.com'],
          subject      = subject, 
          message      = msg,
          mail = 'YAHOO')
    print "Email Sent"
    """
    #getCurrentPositions()
    #df = getLastTrades(startDate='20160101',endDate='20160701')
    #print df
    
    sa = StockAnalytics()
    print sa.getProfitLoss('20030101', '20160928')
    sa.tradeHistory.to_csv("trades.csv")
    print sa.currPositions
