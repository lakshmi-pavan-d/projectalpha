'''
Created on May 7, 2015

@author: pdaggubati
Interface class from TD Ameritrade REST API
'''
import requests, urllib, struct
from datetime import *
from collections import OrderedDict
import xml.etree.ElementTree as ET
import pandas as pd

baseURL     =   'https://apis.tdameritrade.com/apps/100/'
source      =   'KRYA'
version     =   '1.0'
userName    =   'yarla26301'
password    =   'bujjibobby12'
methodKey   =   {
                 'LogIn': 'LogIn?',
                 'LogOut': 'LogOut?',
                 'PriceHistory': 'PriceHistory?',
                 'BalancesAndPositions': 'BalancesAndPositions?',
                 'History': 'History?'
                 }

customHeader =  {'Content-Type': 'application/x-www-form-urlencoded'}


class TDAPI(object):
    
    '''
    Refer to TD Ameritrade API documentation
    '''
    def __init__(self, userName = userName, password = password):
        self.errorMsg = '' 
        self.statuscode = ''
        self.status = ''
        self.sessionid= None
        self.data={}
        self.positions={}
        self.tradeHistory={}
                
    def doAPICall(self, url, payload=None, headers = None):
        print 'url in doAPI call: %s'%(url)
        self.request = url
        r = requests.post(url, data = payload, headers = headers)
        self.response = r.text
        
        if r.status_code != requests.codes.ok:
            self.status = 'FAIL'
            self.statuscode = r.status_code
            self.errorMsg = r.text
            return

        if url.find("PriceHistory?")== -1:
            xmlR = ET.fromstring(r.text)
            #print 'xml response : %s'%r.text
            self.parsedResponse = xmlR
            self.status = xmlR.find('result').text
            self.status_code = r.status_code
            if self.status == 'FAIL':
                self.errorMsg =  xmlR.find('error').text        
                return

        if url.find("LogIn?") != -1:
            self.sessionid = xmlR.find('xml-log-in/session-id').text
        
        ## Response from pricehistory call is binary data and need a separate parser
        ## Encode response into HEX and decode bytes. Refer to TD Documentation
        if url.find('PriceHistory?') != -1:
            hexResponse = r.content.encode('hex')   
            pricehistory = {}
            numSymbols = struct.unpack('!i', hexResponse[0:8].decode('hex'))[0]
            index = 8
            
            for i in range(numSymbols):
                symbolLength = struct.unpack('!h', hexResponse[index: index+4].decode('hex'))[0]
                index  = index + 4
                
                symbol = ''                
                for j in range(symbolLength):
                    symbol += struct.unpack('!s', hexResponse[index: index + 2].decode('hex'))[0]
                    index = index + 2
                
                pricehistory[symbol] = OrderedDict()
                errorCode = struct.unpack('!B', hexResponse[index: index+2].decode('hex'))[0]
                index+= 2
                
                numBars = struct.unpack('!i', hexResponse[index: index+8].decode('hex'))[0]
                index = index + 8                           
                
                if errorCode == 0:
                    pricehistory[symbol]['status'] = 'SUCCESS'
                    data = OrderedDict()
                    for j in range(numBars):
                        close   = struct.unpack('!f', hexResponse[index: index+8].decode('hex'))[0]
                        high    = struct.unpack('!f', hexResponse[index+8: index+16].decode('hex'))[0]
                        low     = struct.unpack('!f', hexResponse[index+16: index+24].decode('hex'))[0]
                        open    = struct.unpack('!f', hexResponse[index+24: index+32].decode('hex'))[0]
                        volume  = struct.unpack('!f', hexResponse[index+32: index+40].decode('hex'))[0]               
                        times   = struct.unpack('!q', hexResponse[index+40: index+56].decode('hex'))[0]
                        # API returns one day lagged days "Weird". To be checked
                        times   = datetime.fromtimestamp(times/1000.0) + timedelta(days=0)
                        data[times] = {'close':close, 'high': high, 'low': low, 'open': open, 'volume': volume}
                        index   = index + 56
                    pricehistory[symbol]['data'] = data
                else:
                    pricehistory[symbol]['status'] = 'FAIL'
                index = index + 4
            self.data = pricehistory
            return 
            
    def LogIn(self):
        if self.sessionid == None:
            urlParams       = {'source': source, 'version': version}
            payloadLogIn    = {'source': source, 'version': version, 'userid': userName, 'password': password}
            url = baseURL + methodKey['LogIn'] + urllib.urlencode(urlParams)
            self.doAPICall(url, payload = payloadLogIn, headers = customHeader)
        return self.status

    
    def PriceHistory(self, symbolList, intervalType, intervalDuration, **kwargs):
        urlParams = {'source':source, 'requestidentifiertype': 'SYMBOL',
                     'requestvalue': ", ".join(symbolList),
                     'intervaltype': intervalType,
                     'intervalduration': intervalDuration,
                     'extended':'true'
                     }
        urlParams.update(**kwargs)
        url = baseURL + methodKey['PriceHistory'] + urllib.urlencode(urlParams)
        self.doAPICall(url)
        
        for sym in symbolList:
            if not self.data.has_key(sym):
                self.data[sym] = {'status':'FAIL', 'data':{}}
        return self.status        
    
    def BalancesAndPositions(self, **kwargs):
        urlParams = {'source':source}
        urlParams.update(**kwargs)
        header = {'Cookie':'JSESSIONID=%s'%self.sessionid}
        url = baseURL + methodKey['BalancesAndPositions'] + urllib.urlencode(urlParams)
        self.doAPICall(url, headers=header)

        positionsData={}
        if self.status != 'FAIL':
            # parse positions
            xmlR = ET.fromstring(self.response)
            positions = xmlR.findall('*/stocks/position')
            
            for p in positions:
                temp = {}
                symbol                  = p.find('security/symbol').text
                temp['name']            = p.find('security/description').text
                temp['position_type']   = p.find('position-type').text
                temp['quantity']        = int(p.find('quantity').text)
                temp['avg_buy_price']   = round(float(p.find('average-price').text),2)
                temp['last_price']      = float(p.find('quote/last').text)
                temp['day_return']      = float(p.find('quote/change-percent').text.strip('%'))/100
                temp['total_return']    = (temp['last_price'] - temp['avg_buy_price'])/temp['avg_buy_price']
                positionsData[symbol]   = temp
        
        self.positions = positionsData
        return self.status
    
    def TradesHistory(self, **kwargs):
        urlParams = {'source':source}
        urlParams.update(**kwargs)
        header = {'Cookie':'JSESSIONID=%s'%self.sessionid}
        url = baseURL + methodKey['History'] + urllib.urlencode(urlParams)
        self.doAPICall(url, headers=header)

        tradeHistory={}
        if self.status != 'FAIL':
            # parse positions
            xmlR = ET.fromstring(self.response)
            trades = xmlR.findall('*/transaction-list')
            
            cols = ['symbol','executedDate','buySellCode','assetType','quantity','price','value']
            df = pd.DataFrame([],columns=cols)
            i = 0
            for t in trades:
                temp = []
                temp.append(t.find('symbol').text)
                temp.append(datetime.strptime((t.find('executedDate').text).split(" ")[0],'%Y-%m-%d'))
                temp.append(t.find('buySellCode').text)
                temp.append(t.find('assetType').text)
                temp.append(int(t.find('quantity').text))
                temp.append(round(float(t.find('price').text),2))
                temp.append(round(float(t.find('value').text.replace(',','')),2))
                df.loc[i] = temp
                i= i+1

        self.tradeHistory= df
        return self.status
    
    def LogOut(self):
        urlParams       = {'source':source}
        url = baseURL + methodKey['LogOut'] + urllib.urlencode(urlParams)
        self.doAPICall(url)
        return self.status

if __name__ == "__main__":
    api = TDAPI()
    api.LogIn()

    """
    api.PriceHistory(['GOOGL', 'AAPL'], 'DAILY', 1, startdate='20160923', enddate='20160923')
    print api.status
    print api.errorMsg
    print api.data
    print api.sessionid
    
    status = api.BalancesAndPositions(type='p')
    print api.positions
    import xml.dom.minidom
    f = open('txt.csv','w')
    f.write(api.response)
    f.close()
    xml = xml.dom.minidom.parse('txt.csv') # or xml.dom.minidom.parseString(xml_string)
    pretty_xml_as_string = xml.toprettyxml()
    print pretty_xml_as_string
    api.LogOut()
    """
    
    status = api.TradesHistory(type=1,startdate='20020101', enddate = '20160920')
    print api.tradeHistory
    api.LogOut()
    (api.tradeHistory).to_csv('trades.csv')
    
    