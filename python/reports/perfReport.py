"""
Grab VB Investor portfolios and send out a daily email
"""

import urllib2, datetime
import numpy as np
from utils.Utils import *
import pandas as pd
from StringIO import StringIO
from utils.gFinanceLogin import GoogleFinanceLogin

owner_map = {'LNKD': ['R.Dasari'], 'VLO':['A. Dasari'], 'AAPL': ['R.Kudithipudi'],
             'UCO': ['B.Kolla'], 'UWTI': ['Rajkumar'], 'UGAZ':['Rajkumar'], 
             'MU': ['R.Adusumilli'], 'GPRO': ['P.Daggubati'], 'NKE':['A.Aluri'],
             'KORS': ['P.Krothapalli'], 'CVX': ['N.Kommareddy'], 'HTZ':['N.Kommareddy'],
             'FLML': ['K.Yarlagadda'], 'AMD':['K.Yarlagadda'], 'UA': ['R.Kudithipudi'],
             'ERX': ['R.Aluri'], 'NFLX': ['R.Yerneni'], 'TSLA':['R.Yerneni'],
             'BABA':['S.Dasari'], 'MBLY':['S.Dasari']
             }

to_add = ['vbinvestors@googlegroups.com','rdurga@live.com']
#to_add = ['pdaggubati@financialengines.com']

c= GoogleFinanceLogin('pavan2812@gmail.com','Pavan1985')
response = urllib2.urlopen('https://www.google.com/finance/portfolio?pid=1&output=csv&action=view&pview=pview&ei=CDX0VZm8E4q9igLEvKZ4&authuser=1')
portStream = response.read()
df = pd.DataFrame.from_csv(StringIO(portStream),index_col=1)
df.reset_index(inplace=True)

df_owner_map = pd.DataFrame(owner_map).T.reset_index()
df_owner_map.columns = ['Symbol','Analyst']
df_m = df.merge(df_owner_map, 'left', 'Symbol')
df_m['Purchase_price'] = df_m['Cost basis'].div(df_m['Shares'])

df_m.columns = ['Symbol', 'Name', 'Last_price', 'Change', 'Shares', 'Cost_basis',
       'Mkt_value', 'Gain', 'Gain_percent', 'Day_gain', 'Overall_return',
       'Analyst', 'Purchase_price']

df_m = df_m[['Symbol','Name','Last_price','Purchase_price','Cost_basis','Mkt_value','Overall_return','Analyst']]
df_m.sort(['Overall_return'], ascending=[0],inplace=True)
df_m.dropna(inplace=True)

# add portfolio return at the end
sum = df_m[['Cost_basis','Mkt_value']].sum(axis=0,numeric_only=True)
df_m.loc[-1] = ['','Portfolio_return',np.NaN,np.NaN,sum['Cost_basis'],sum['Mkt_value'],100*(sum['Mkt_value'] - sum['Cost_basis'])/sum['Cost_basis'],'']

fn = lambda x: "{0:.1f}%".format(x)
fn1 = lambda x: "{0:.2f}".format(x)
fn2 = lambda x: "{0:.0f}".format(x)
msg = df_m.to_html(justify='left',formatters={'Last_price':fn1, 'Purchase_price': fn1, 'Gain_percent':fn,'Overall_return':fn,'Cost_basis':fn2, 'Mkt_value':fn2},index=False)
subject = "Fantasy Stocks Performance - " + datetime.datetime.today().strftime('%b %d, %Y')   
sendHTMLEmail(from_addr= 'pavan_daggubati@yahoo.co.in', 
          to_addr_list = to_add,
          subject      = subject, 
          message      = msg,
          mail = 'YAHOO')
print "Email Sent"
