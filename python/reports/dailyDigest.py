import os, sys
path_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir))

try:
    x = sys.path.index(path_dir)
except:
    sys.path.append(path_dir)

from core.stockAnalytics import StockAnalytics
from utils.Utils import *
from datetime import datetime, timedelta, date
from pytz import timezone
sa = StockAnalytics()

# Globals
csv_f = os.path.dirname(os.path.realpath(__file__)) + '/input_tickers.csv'
#csv_f = os.path.dirname(os.path.realpath(__file__)) + '/inp.txt'
pd.set_option('display.max_colwidth', 150)

    
if __name__ == "__main__":
    
    # Read input list
    print "#########################################################################"
    print 'Daily digest run %s'%(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
    f = open(csv_f,'r')
    tickers = f.read().split('\n')
    tickers = [x.strip() for x in tickers]
    f.close()

    rDt = datetime.now(timezone('US/Pacific'))
    #rDt = date(2016,9,26)
    today   = rDt.strftime('%Y%m%d')
    prevday = priorBDay(dt=rDt,nPrior=-1).strftime('%Y%m%d')
    prevwk  = priorBDay(dt=rDt,nPrior=-5).strftime('%Y%m%d')
    prevmt  = priorBDay(dt=rDt,nPrior=-22).strftime('%Y%m%d')
    prevqt  = priorBDay(dt=rDt,nPrior=-64).strftime('%Y%m%d')
    prevyr  = priorBDay(dt=rDt,nPrior=-252).strftime('%Y%m%d')
    prev2yr  = priorBDay(dt=rDt,nPrior=-504).strftime('%Y%m%d')
    prev5yr  = priorBDay(dt=rDt,nPrior=-1260).strftime('%Y%m%d')
    
    stockData = sa.getStockData(tickers, rDt)
    df_holdings  = sa.getCurrentPositions()
    df_tradeHistory = sa.getLastTrades(startDate=prevyr, endDate=prevday)

    # join trade history data
    fn = lambda x: "{0:.1f}%".format(x*100)
    df_moversall = pd.merge(sa.getMovers(stockData, today, prevday, nThres=0),df_tradeHistory, left_index=True, right_index = True, how='left')
    df_moversday= pd.merge(sa.getMovers(stockData, today, prevday, nThres = 0.05),df_tradeHistory, left_index=True, right_index = True, how='left')
    df_moverswk = pd.merge(sa.getMovers(stockData, today, prevwk, nThres = 0.10),df_tradeHistory, left_index=True, right_index = True, how='left')
    df_moversmt = pd.merge(sa.getMovers(stockData, today, prevmt, nThres = 0.20),df_tradeHistory, left_index=True, right_index = True, how='left')
    df_moversqt = pd.merge(sa.getMovers(stockData, today, prevqt, nThres = 0.25),df_tradeHistory, left_index=True, right_index = True, how='left')
    df_moversyr = pd.merge(sa.getMovers(stockData, today, prevyr, nThres = 0.50),df_tradeHistory, left_index=True, right_index = True, how='left')
    df_movers2yr = pd.merge(sa.getMovers(stockData, today, prev2yr, nThres = 1.00),df_tradeHistory, left_index=True, right_index = True, how='left')
    df_movers5yr = pd.merge(sa.getMovers(stockData, today, prev5yr, nThres = 2.50),df_tradeHistory, left_index=True, right_index = True, how='left')
           
    msg = ''
    msg += '<b>KY Portfolio </b>'
    msg += df_holdings.to_html(justify='left',formatters={'day_return':fn,'total_return':fn},na_rep='') + "<br />"
    
    msg += '<b>5%+ move: past day (' +  prevday + ', ' + today + ')</b>'
    if len(df_moversday.index) >0:
        msg += df_moversday.to_html(justify='left',formatters={'period_return':fn},na_rep='') + "<br />"
    else:
        msg += '<br><i>NONE</i><br><br>'
        print "no greater than 5% moves in the past day"

    msg += '<b>10%+ move: past week (' +  prevwk + ', ' + today + ')</b>'
    if len(df_moverswk.index) >0:
        msg += df_moverswk.to_html(justify='left',formatters={'period_return':fn},na_rep='') + "<br />"
    else:
        msg += '<br><i>NONE</i><br><br>'
        print "no greater than 10% move in the past week"

    msg += '<b>20%+ move: past month (' +  prevmt + ', ' + today + ')</b>'
    if len(df_moversmt.index) >0:
        msg += df_moversmt.to_html(justify='left',formatters={'period_return':fn},na_rep='') + "<br />"
    else:
        msg += '<br><i>NONE</i><br><br>'
        print "no greater than 20% move in the past month"

    msg += '<b>25%+ move: past quarter (' +  prevqt + ', ' + today + ')</b>'
    if len(df_moversqt.index) >0:
        msg += df_moversqt.to_html(justify='left',formatters={'period_return':fn},na_rep='') + "<br />"
    else:
        msg += '<br><i>NONE</i><br><br>'
        print "no greater than 25% move in the past quarter"

    msg += '<b>50%+ move: past year (' +  prevyr + ', ' + today + ')</b>'
    if len(df_moversyr.index) >0:
        msg += df_moversyr.to_html(justify='left',formatters={'period_return':fn},na_rep='') + "<br />"
    else:
        msg += '<br><i>NONE</i><br><br>'
        print "no greater than 50% move in the past year"

    msg += '<b>100%+ move: past 2 years (' +  prev2yr + ', ' + today + ')</b>'
    if len(df_moversyr.index) >0:
        msg += df_movers2yr.to_html(justify='left',formatters={'period_return':fn},na_rep='') + "<br />"
    else:
        msg += '<br><i>NONE</i><br><br>'
        print "no greater than 100% move in the past 2 years"
    
    msg += '<b>250%+ move: past 5 years (' +  prev5yr + ', ' + today + ')</b>'
    if len(df_moversyr.index) >0:
        msg += df_movers5yr.to_html(justify='left',formatters={'period_return':fn},na_rep='') + "<br />"
    else:
        msg += '<br><i>NONE</i><br><br>'
        print "no greater than 250% move in the past 5 years"
    
    
    threshold = 0.1
    df_corpevents = sa.getCorpEvents(stockData)
    df_52wkhighlow = sa.get52WkHighLowStocks(stockData, th=threshold)

    msg += '<b>Stocks near 52 Week Low - Threshold %s</b>'%threshold
    msg += df_52wkhighlow[1].to_html(justify='left') + "<br />"
    msg += '<b>Stocks near 52 Week High- Threshold %s</b>'%threshold
    msg += df_52wkhighlow[0].to_html(justify='left') + "<br />"

    msg += '<b>Upcoming Earnings - Next two weeks</b>'
    if len(df_corpevents.index)>0:
        msg += df_corpevents.to_html(justify='left')  + "<br />"
    else:
        msg += '<br><i>NONE</i><br><br>'
        print "no earning reports in the next 2 weeks"
    
    subject = "Daily Digest " + rDt.strftime('%b %d, %Y')   
    sendHTMLEmail(from_addr= 'pavan2812@gmail.com', 
          to_addr_list = ['lakshmi.pavan.d@gmail.com'],
          #to_addr_list = ['navyaswetha@gmail.com','kyarlagadda@gmail.com','lakshmi.pavan.d@gmail.com'],
          subject      = subject, 
          message      = msg,
          mail = 'FE')
    print "DailyDigest Email Sent"
    
    ##### data for all stocks
    msg = '<b>All stocks: past day (' +  prevday + ', ' + today + ')</b>'
    msg += df_moversall.to_html(justify='left',formatters={'period_return':fn},na_rep='') + "<br />"
    subject = "Daily Digest2 " + rDt.strftime('%b %d, %Y')   
    sendHTMLEmail(from_addr= 'pavan2812@gmail.com', 
          to_addr_list = ['lakshmi.pavan.d@gmail.com'],
          #to_addr_list = ['navyaswetha@gmail.com','kyarlagadda@gmail.com','lakshmi.pavan.d@gmail.com'],
          subject      = subject, 
          message      = msg,
          mail = 'FE')
    print "DailyDigest2 Email Sent"
