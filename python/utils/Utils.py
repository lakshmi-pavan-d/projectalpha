import smtplib
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
import base64
import pandas as pd
from pandas.tseries.holiday import USFederalHolidayCalendar
from pandas.tseries.offsets import CustomBusinessDay
from pandas.tseries.offsets import CustomBusinessMonthEnd


access = {
          'YAHOO'   :{'SERVER': 'smtp.mail.yahoo.com:587','LOGIN':'pavan_daggubati', 'PASSWORD':'bGFrc2htaW5hdmFw' },
          'GOOGLE'  :{'SERVER': 'smtp.gmail.com:587','LOGIN':'pavan2812@gmail.com', 'PASSWORD':'bXlnbWFpbDIwMTQ=' },
          'FE'      :{'SERVER':'mailhost.pa.fngn.com','LOGIN':'','PASSWORD':''}
          }

def sendHTMLEmail(from_addr, to_addr_list, subject, message, mail ='YAHOO'):
    
    headers = "\r\n".join(["from: " + from_addr,
                       "subject: " + subject,
                       "to: " + ','.join(to_addr_list),
                       "mime-version: 1.0",
                       "content-type: text/html"])

    # body_of_email can be plaintext or html!                    
    content = headers + "\r\n\r\n" + message
    
    smtpserver = access[mail]['SERVER']
    login      = access[mail]['LOGIN']
    password   = base64.b64decode(access[mail]['PASSWORD'])
    
    server = smtplib.SMTP(smtpserver)
    if mail == 'YAHOO' or mail=='GOOGLE':
        server.ehlo()
        server.starttls()
        server.login(login,password)
    server.sendmail(from_addr, to_addr_list, content)
    server.quit()


def sendText(from_addr, to_addr_list, subject, msg, mail ='YAHOO'):
    smtpserver = access[mail]['SERVER']
    login      = access[mail]['LOGIN']
    password   = base64.b64decode(access[mail]['PASSWORD'])
    
    server = smtplib.SMTP(smtpserver)
    if mail == 'YAHOO' or mail=='GOOGLE':
        server.ehlo()
        server.starttls()
        server.login(login,password)
    server.sendmail(from_addr, to_addr_list, msg)
    server.quit()


def priorBDay(dt=pd.datetime.today(),nPrior = -1):
    bday_us = CustomBusinessDay(calendar=USFederalHolidayCalendar())
    return (dt + nPrior*bday_us)

def lastBDayMonth(dt=pd.datetime.today()):
    bmth_us = CustomBusinessMonthEnd(calendar=USFederalHolidayCalendar())
    return (dt + bmth_us)