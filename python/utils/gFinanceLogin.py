"""
gvoice.py
Created by: Scott Hillman
http://www.everydayscripting.blogspot.com
This module comes as is an with no warranty. 
You are free to use, modify and distribute this 
code however you wish, but I ask that if you post 
it anywhere, you at least make reference to me and
my blog where you acquired it.
"""

import sys
import re
import urllib
import urllib2
import json

class GoogleFinanceLogin:

    def __init__(self, email = None, password = None):        
        if email is None:
            email = raw_input("Please enter your Google Account username: ")
        if password is None:
            import getpass
            password = getpass.getpass("Please enter your Google Account password: ")

        # Set up our opener
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor())
        urllib2.install_opener(self.opener)

        # Define URLs
        self.login_page_url = 'https://accounts.google.com/ServiceLogin?service=finance'
        self.authenticate_url = 'https://accounts.google.com/ServiceLoginAuth?service=finance'
        self.gf_home_page_url = 'https://finance.google.com'    

        # Load sign in page
        login_page_contents = self.opener.open(self.login_page_url).read()

        # Find GALX value
        galx_match_obj = re.search(r'name="GALX"\s*type="hidden"\n\s*value="([^"]+)"', login_page_contents, re.IGNORECASE)

        galx_value = galx_match_obj.group(1) if galx_match_obj.group(1) is not None else ''

        # Set up login credentials
        login_params = urllib.urlencode({
            'Email' : email,
            'Passwd' : password,
            'continue' : 'https://www.google.com/finance',
            'GALX': galx_value
        })

        # Login
        self.opener.open(self.authenticate_url, login_params)