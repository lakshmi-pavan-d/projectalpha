from stockAnalytics import *
import os
from Utils import *
from datetime import timedelta

# Globals
csv_f = os.path.dirname(os.path.realpath(__file__)) + '/input_tickers.csv'
#csv_f = os.path.dirname(os.path.realpath(__file__)) + '/small.csv'
pd.set_option('display.max_colwidth', 150)

    
if __name__ == "__main__":
    
    # Read input list
    print 'Daily digest run %s'%(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
    f = open(csv_f,'r')
    tickers = f.read().split('\n')
    tickers = [x.strip() for x in tickers]
    f.close()

    rDt = datetime.today()
    today   = rDt.strftime('%Y%m%d')
    #prevyr  = priorBDay(dt=rDt,nPrior=-252).strftime('%Y%m%d')
    prevyr = '20141231'
    
    stockData = getStockData(tickers, rDt, histDates=[today,prevyr])
    fn = lambda x: "{0:.1f}%".format(x*100)
    df_moversyr = getMovers(stockData, today, prevyr, nThres = 0)
    
    msg = ''
    msg += '<b>50%+ move: past year (' +  prevyr + ', ' + today + ')</b>'
    if len(df_moversyr.index) >0:
        msg += df_moversyr.to_html(justify='left',formatters={'period_return':fn}) + "<br />"
    else:
        msg += '<br><i>NONE</i><br><br>'
        print "no greater than 50% move in the past year"
    
    subject = "Daily Digest " + rDt.strftime('%b %d, %Y')   
    sendHTMLEmail(from_addr= 'pavan_daggubati@yahoo.co.in', 
          to_addr_list = ['lakshmi.pavan.d@gmail.com'],
          subject      = subject, 
          message      = msg,
          mail = 'YAHOO')
    print "Email Sent"